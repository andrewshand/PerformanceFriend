package com.theandyshand;
import java.util.UUID;

import com.bitwig.extension.api.PlatformType;
import com.bitwig.extension.controller.AutoDetectionMidiPortNamesList;
import com.bitwig.extension.controller.ControllerExtensionDefinition;
import com.bitwig.extension.controller.api.ControllerHost;

public class PerformanceFriendExtensionDefinition extends ControllerExtensionDefinition
{
   private static final UUID DRIVER_ID = UUID.fromString("552dffa0-590d-4392-9ceb-8c5fa2a2bb3c");
   
   public PerformanceFriendExtensionDefinition()
   {
   }

   @Override
   public String getName()
   {
      return "Performance Friend";
   }
   
   @Override
   public String getAuthor()
   {
      return "theandyshand";
   }

   @Override
   public String getVersion()
   {
      return "0.1";
   }

   @Override
   public UUID getId()
   {
      return DRIVER_ID;
   }
   
   @Override
   public String getHardwareVendor()
   {
      return "Andy Shand";
   }
   
   @Override
   public String getHardwareModel()
   {
      return "5'8\"";
   }

   @Override
   public int getRequiredAPIVersion()
   {
      return 3;
   }

   @Override
   public int getNumMidiInPorts()
   {
      return 2;
   }

   @Override
   public int getNumMidiOutPorts()
   {
      return 2;
   }

   @Override
   public void listAutoDetectionMidiPortNames(final AutoDetectionMidiPortNamesList list, final PlatformType platformType)
   {

   }

   @Override
   public PerformanceFriendExtension createInstance(final ControllerHost host)
   {
      return new PerformanceFriendExtension(this, host);
   }
}
