package com.theandyshand;

import com.bitwig.extension.api.util.midi.ShortMidiMessage;
import com.bitwig.extension.callback.ShortMidiMessageReceivedCallback;
import com.bitwig.extension.controller.api.*;

public class Launchkey61 {

    private MidiIn midiInPort0, midiInPort1;
    private MidiOut midiOutPort1;
    private TrackBank trackBank;
    private Transport mTransport;
    PerformanceFriendExtension extension;
    ControllerHost host;

    Launchkey61(PerformanceFriendExtension extension) {
        this.extension = extension;
        trackBank = extension.getTrackBankSection();
        host = extension.getHost();

        for (int i = 0; i < trackBank.getSizeOfBank(); i++) {
            Track t = trackBank.getItemAt(i);
            t.trackType().markInterested();
        }

        midiOutPort1 = host.getMidiOutPort(1);
        mTransport = host.createTransport();

        midiInPort0 = host.getMidiInPort(0);
        midiInPort1 = host.getMidiInPort(1);

        midiInPort0.setMidiCallback((ShortMidiMessageReceivedCallback) this::onMidi0);
        midiInPort1.setMidiCallback((ShortMidiMessageReceivedCallback) this::onMidi1);

        midiInPort0.createNoteInput("Keys", "B0????", "80????", "90????", "B001??", "D0????", "E0????");

        // set the keyboard into InControl mode
        midiOutPort1.sendMidi(159, 12,127);

        updateLights();
        host.showPopupNotification("Launchkey 61 Initialized");
    }

    void exit()
    {
        midiOutPort1.sendMidi(159, 12,0);
    }

    private void onMidi0(ShortMidiMessage msg)
    {
//      println("Port 0");
//      println("Channel: " + msg.getChannel());
//      println("Data1:" + msg.getData1());
//      println("Data2:" + msg.getData2());
//      println("Status:" + msg.getStatusByte());
    }

    private void onFaderChange(int index, byte value) {

    }

    private void onKnobChange(int index, byte value) {

    }

    private int colorForPadIndex(int index) {
        return (new int[] {6, 9, 109, 22, 19, 41, 54, 58})[index % 8];
    }

    private int noteForPad(int intended) {
        if (intended < 8) {
            return intended + 96;
        }
        return intended + 112 - 8;
    }

    private void onPadButtonChange(int index, boolean on) {
        if (!on) {
            return;
        }

        if (index == 0) {
            // arm everything, allow to kill sustain
            TrackBank bank = extension.getTrackBankSection();
            for (int i = 0; i < bank.getSizeOfBank(); i++) {
                if (!bank.getItemAt(i).trackType().get().equals("Audio")) {
                    bank.getItemAt(i).getArm().set(true);
                }
            }
        }
        else {
            extension.armEventsUpToNow();
        }
    }

    private void updateLights() {
        for (int i = 0; i < 17; i++) {
            if (i == 8) {
                continue; // skip the irrelevant button
            }
            int correctedIndex = i < 8 ? i : i - 1; // the extra button has pushed us up one
            int note = noteForPad(correctedIndex);
            int color = colorForPadIndex(correctedIndex);
            midiOutPort1.sendMidi(0x9F, note, color);
        }
    }

    private void onPadSecondaryButtonChange(int index, boolean on) {
        if (!on) {
            return;
        }
    }

    private void onFaderButtonChange(int index, boolean on) {

    }

    private void onMasterFaderChange(byte value) {

    }

    private void onMasterButtonChange(boolean on) {
        if (!on) {
            return;
        }
    }

    /** Called when we receive short MIDI message on port 1. */
    private void onMidi1(ShortMidiMessage msg)
    {

        if (msg.getStatusByte() == 159 && msg.getData2() == 0) {
            // in control change, force them back into control
            //midiOutPort1.sendMidi(159, 14, 127); // sliders
            //midiOutPort1.sendMidi(159, 13, 127); // pots
            //midiOutPort1.sendMidi(159, 15, 127); // drum pads
        }
        if (msg.isNoteOn() || msg.isNoteOff()) {
            if (msg.getData1() >= 96 && msg.getData1() <= 103) {
                // first row of pads
                onPadButtonChange(msg.getData1() - 96, msg.isNoteOn());
            }
            if (msg.getData1() >= 112 && msg.getData1() <= 119) {
                // second row of pads
                onPadButtonChange(msg.getData1() - 112 + 8, msg.isNoteOn());
            }
            if (msg.getData1() == 104 || msg.getData1() == 120) {
                // buttons next to pads
                onPadSecondaryButtonChange(msg.getData1() == 104 ? 0 : 1, msg.isNoteOn());
            }
        }
        if (msg.isControlChange()) {
            if (msg.getData1() >= 21 && msg.getData1() <= 28) {
                onKnobChange(msg.getData1() - 21, (byte) msg.getData2());
            }
            if (msg.getData1() >= 41 && msg.getData1() <= 48) {
                onFaderChange(msg.getData1() - 41, (byte) msg.getData2());
            }
            if (msg.getData1() >= 51 && msg.getData1() <= 58) {
                onFaderButtonChange(msg.getData1() - 51, msg.getData2() == 127);
            }
            if (msg.getData1() == 7) {
                onMasterFaderChange((byte) msg.getData2());
            }

            if (msg.getData2() == 127) {
                // 127 == key down only
                if (msg.getData1() == 59) {
                    onMasterButtonChange(msg.getData2() == 127);
                }
                if (msg.getData1() == 112) {
                    mTransport.rewind();
                }
                if (msg.getData1() == 113) {
                    mTransport.fastForward();
                }
                if (msg.getData1() == 114) {
                    mTransport.stop();
                }
                if (msg.getData1() == 115) {
                    mTransport.play();
                }
                if (msg.getData1() == 116) {
                    mTransport.isArrangerLoopEnabled().toggle();
                }
                if (msg.getData1() == 117) {
                    mTransport.isArrangerRecordEnabled().toggle();
                }
            }

        }
    }

}
