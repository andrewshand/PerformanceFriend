package com.theandyshand;

import com.bitwig.extension.controller.api.Track;
import com.bitwig.extension.controller.api.TrackBank;

import java.util.HashMap;

public class ArmManager {

    private HashMap<String, String> armedByDevice = new HashMap<>();
    private TrackBank bank;
    public final static int TRACK_BANK_SIZE = 128;
    private HashMap<String, Track> nameTrackHashMap = new HashMap<>();
    private boolean firstRun = true;
    PerformanceFriendExtension extension;

    ArmManager(TrackBank bank) {
        this.bank = bank;
        this.extension = PerformanceFriendExtension.getInstance();

        for (int i = 0; i < TRACK_BANK_SIZE; i++) {
            Track t = bank.getItemAt(i);
            t.getArm().markInterested();
            t.name().addValueObserver(name -> {
                // we don't want to run this for every
                // single track when adding initial observers
                if (!firstRun) {
                    updateTrackHashMap();
                }
            });
        }
        firstRun = false;
        updateTrackHashMap();
    }

    void reset() {
        for (int i = 0; i < TRACK_BANK_SIZE; i++) {
            Track t = bank.getItemAt(i);
            t.getArm().set(false);
        }
        armedByDevice.clear();
        updateTrackHashMap();
    }

    void updateTrackHashMap() {
        nameTrackHashMap = new HashMap<>();
        for (int i = 0; i < TRACK_BANK_SIZE; i++) {
            Track t = bank.getItemAt(i);
            nameTrackHashMap.put(t.name().get(), t);
        }
    }

    Track getTrackByName(String name) {
        return nameTrackHashMap.get(name);
    }

    void arm(String device, String trackName) {

        if (armedByDevice.containsKey(device)) {
            if (armedByDevice.get(device).equals(trackName)) {
                // already armed!
                extension.getHost().println("already armed m8!");
                return;
            }
            String armedCurrently = armedByDevice.get(device);
            PerformanceFriendExtension.getInstance().getFakeNoteInput().sendRawMidiEvent(0xB0, 0x40, 0x00);
            PerformanceFriendExtension.getInstance().getFakeNoteInput().sendRawMidiEvent(0xB1, 0x40, 0x00);
            PerformanceFriendExtension.getInstance().getFakeNoteInput().sendRawMidiEvent(0xB2, 0x40, 0x00);
            PerformanceFriendExtension.getInstance().getFakeNoteInput().sendRawMidiEvent(0xB3, 0x40, 0x00);
            PerformanceFriendExtension.getInstance().getFakeNoteInput().sendRawMidiEvent(0xB4, 0x40, 0x00);
            getTrackByName(armedCurrently).getArm().set(false);
            PerformanceFriendExtension.getInstance().getFakeNoteInput().sendRawMidiEvent(0xB0, 0x40, 0x00);
            PerformanceFriendExtension.getInstance().getFakeNoteInput().sendRawMidiEvent(0xB1, 0x40, 0x00);
            PerformanceFriendExtension.getInstance().getFakeNoteInput().sendRawMidiEvent(0xB2, 0x40, 0x00);
            PerformanceFriendExtension.getInstance().getFakeNoteInput().sendRawMidiEvent(0xB3, 0x40, 0x00);
            PerformanceFriendExtension.getInstance().getFakeNoteInput().sendRawMidiEvent(0xB4, 0x40, 0x00);

            armedByDevice.remove(device);
        }

        if (getTrackByName(trackName) == null) {
            extension.getHost().showPopupNotification("Couldn't find track: " + trackName);
            return;
        }

        extension.getHost().println("successfully armed");
        getTrackByName(trackName).getArm().set(true);
        armedByDevice.put(device, trackName);
    }
}
