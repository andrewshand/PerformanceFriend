package com.theandyshand;

import com.bitwig.extension.controller.api.*;
import com.bitwig.extension.controller.ControllerExtension;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;

public class PerformanceFriendExtension extends ControllerExtension
{
   private static PerformanceFriendExtension instance;
   public static PerformanceFriendExtension getInstance() {
      return instance;
   }

   private TrackBank trackBankSection;
   private Application application;
   private MasterTrack masterTrack;
   private CursorDevice currDevice;
   private CursorTrack cursorTrack;
   private Transport mTransport;
   private NoteInput fakeNoteInput;
   private CursorRemoteControlsPage remoteControlsPage;
   Launchkey61 launchkey61;
   ArmManager armManager;
   boolean fileWatching = true;

   double processedUpTo = 0;
   TrackDescription currentTrackDescription;
   private static Path BASE_PATH;

   protected PerformanceFriendExtension(final PerformanceFriendExtensionDefinition definition, final ControllerHost host)
   {
      super(definition, host);
      instance = this;
   }

   @Override
   public void init() {
      try {

         BASE_PATH = FileSystems.getDefault().getPath(
                 System.getProperty("user.home"),
                 "Dropbox/Music/Live Sets/Friend Files"
         ).toRealPath();
      } catch (IOException e) {
         println("Bad biscuits creating initial path: " + e.toString());
         e.printStackTrace();
      }
      final ControllerHost host = getHost();

      cursorTrack = host.createCursorTrack(0, 0);
      currDevice = cursorTrack.createCursorDevice("primary", "Primary", 0, CursorDeviceFollowMode.FOLLOW_SELECTION);

      mTransport = host.createTransport();
      mTransport.tempo().markInterested();
      mTransport.tempo().displayedValue().markInterested();
      mTransport.getPosition().markInterested();
      mTransport.isPlaying().markInterested();
      trackBankSection = host.createTrackBank(ArmManager.TRACK_BANK_SIZE, 0, 0);
      masterTrack = host.createMasterTrack(1);

      application = host.createApplication();
      application.projectName().markInterested();
      application.projectName().addValueObserver(projectName -> {
         if (projectName.length() == 0) {
            return;
         }
         println("updating for " + projectName);
         armManager.reset();
         currentTrackDescription = new TrackDescription(projectName, BASE_PATH + "/" + projectName + ".json");
      });

      armManager = new ArmManager(trackBankSection);

      initiateFileWatcher();
      this.armTracksForCurrentTime();

      mTransport.isPlaying().addValueObserver(playing -> {
         if (playing) {
            processedUpTo = mTransport.getPosition().get();
            if (processedUpTo < 1) {
               processedUpTo = 0;
            }
            armEventsUpToNow();
         }
      });

      this.fakeNoteInput = host.getMidiInPort(0).createNoteInput( "Keyboard", "FFFFFF" );
      this.fakeNoteInput.setShouldConsumeEvents( false );
      host.showPopupNotification("Performance Friend Initialized");

      launchkey61 = new Launchkey61(this);
   }

   public NoteInput getFakeNoteInput() {
      return this.fakeNoteInput;
   }

   double getTempo() {
      String displayed = mTransport.tempo().displayedValue().get();
      return Double.parseDouble(displayed.substring(0, displayed.length() - 4));
   }

   void armEventsUpToNow() {
      getHost().println("arm events up to now!");
      armEventsUpTo(mTransport.getPosition().get());
   }

   void armEventsUpTo(double beats) {
      if (currentTrackDescription == null) {
         return;
      }

      armManager.reset();
      List<TrackDescription.TrackDescriptionEvent> events = currentTrackDescription.getLastEventsBeforeBeat(beats);
      for (TrackDescription.TrackDescriptionEvent event : events) {
         if (!event.type.equals("arm")) {
            continue;
         }

         getHost().println("arming: " + event.trackName + " for device: " + event.device);
         println("arming: " + event.trackName + " for device: " + event.device);
         armManager.arm(event.device, event.trackName);
      }
   }

   private void armTracksForCurrentTime() {
      if (!mTransport.isPlaying().get()) {
         getHost().println("not playing");
         return;
      }
      if (currentTrackDescription == null) {
         getHost().errorln("no track description");
         return;
      }

      if (fileWatching) {
         checkFileChanges();
      }

      getHost().println("arm track attempt!");
      double currentTime = mTransport.getPosition().get();
      // min to processed up to incase we start late, don't want to miss events. it's okay if we repeat some
      double start = Math.min(currentTime, processedUpTo);
      double end = start + 1; // 1 beat ahead in the future

      println("start and end: " + start + ", " + end);
      List<TrackDescription.TrackDescriptionEvent> events = currentTrackDescription.getEventsIntersectingBeats(start, end);

      for (TrackDescription.TrackDescriptionEvent event : events) {
         if (!event.type.equals("arm")) {
            continue;
         }

         getHost().println("arming: " + event.trackName + " for device: " + event.device);
         println("arming: " + event.trackName + " for device: " + event.device);
         armManager.arm(event.device, event.trackName);
      }

      println("scheduling another task");
      // attempt to reschedule again 1 beats time in the future, assuming no crazy tempo changes. not a problem if we're slightly
      // forward anywho
      double beatsPerSecond = (getTempo() / 60d);
      println("BPS: " + beatsPerSecond);
      double secondsPerBeat = 1d / beatsPerSecond;

      // try to wait the right amount of time to fall right on the next beat,
      // but add a little bit of time before incase we miss it
      double beatsToWait = 1 - (currentTime % 1) - 0.25d;
      int delayMs = (int) Math.max(secondsPerBeat * beatsToWait * 1000, 10);
      println("delay: " + delayMs);

      processedUpTo = end;
   }

   @Override
   public void exit()
   {
      getHost().showPopupNotification("Performance Friend Exited");
      watchKey.cancel();
      try {
         watchService.close();
      } catch (IOException e) {
         getHost().errorln("error closing watch service: " + e.toString());
      }
   }

   @Override
   public void flush()
   {
      armTracksForCurrentTime();
   }

   void println(String msg) {
//      getHost().println(msg);
   }

   private WatchKey watchKey;
   private WatchService watchService;

   private void checkFileChanges() {
      println("starting file check");
      for (WatchEvent<?> event : watchKey.pollEvents()) {
         if (this.currentTrackDescription != null) {
            this.currentTrackDescription.loadFromFile();
         }
         break;
      }
      // reset the key
      boolean valid = watchKey.reset();
      if (!valid) {
         println("Key has been unregistered");
      }

      println("finished file check");
   }

   public TrackBank getTrackBankSection() {
      return trackBankSection;
   }

   private void initiateFileWatcher() {
      try {
         watchService = FileSystems.getDefault().newWatchService();
         watchKey = BASE_PATH.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE);
      } catch (IOException e) {
         e.printStackTrace();
         println("bad biscuits occurred creating file watcher" + e.toString());
      }
   }
}
