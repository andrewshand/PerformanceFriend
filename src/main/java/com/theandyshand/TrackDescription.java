package com.theandyshand;

import com.bitwig.extension.controller.api.ControllerHost;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class TrackDescription {

    String filePath;
    String projectName;
    TrackDescriptionData data;
    HashMap<Integer, Integer> knownBeatIndexes = new HashMap<>();
    PerformanceFriendExtension extension;
    HashSet<String> instruments = new HashSet<String>() {{
        add("guitar");
        add("keyboard");
        add("mic");
    }};

    TrackDescription(String projectName, String filePath) {
        this.projectName = projectName;
        this.filePath = filePath;
        this.extension = PerformanceFriendExtension.getInstance();
        loadFromFile();
    }

    void loadFromFile() {
        try {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new FileReader(this.filePath));
            data = gson.fromJson(reader, TrackDescriptionData.class);
            extension.println(data.toString());
            data.events.sort((a, b) -> {
                if (a.beat == b.beat) { return 0; }
                return a.beat < b.beat ? -1 : 1;
            });
            knownBeatIndexes.clear();

            int eventIndex = 0;
            int lastKnownBeat = 0;
            for (TrackDescriptionEvent event : data.events) {
                extension.println("event " + eventIndex);
                int intBeat = (int) event.beat;
                if (!knownBeatIndexes.containsKey(intBeat)) {
                    while (lastKnownBeat <= intBeat) {
                        extension.println("in a while");
                        knownBeatIndexes.put(lastKnownBeat, eventIndex);
                        lastKnownBeat++;
                    }
                }
                eventIndex++;
            }
            extension.println("finished adding events");
        } catch (FileNotFoundException e) {
            ControllerHost host = PerformanceFriendExtension.getInstance().getHost();
            host.errorln("Error loading JSON: " + e.toString());
            PerformanceFriendExtension.getInstance().getHost().showPopupNotification("Error loading " + this.filePath);
        }
    }

    /**
     * Returns the last events uniqued per device before a given beat, ready to manually
     * set the currently armed tracks from a given point in a song. Panic button!
     * @param beat
     * @return
     */
    List<TrackDescriptionEvent> getLastEventsBeforeBeat(double beat) {
        ArrayList<TrackDescriptionEvent> list = new ArrayList<>();
        if (data == null || data.events.size() == 0) {
            extension.println("No events");
            return list;
        }

        HashSet<String> toFind = new HashSet<>(this.instruments);
        int intBeat = (int) beat;
        int eventIndex = knownBeatIndexes.getOrDefault(intBeat, data.events.size() - 1);

        while (eventIndex >= 0 && toFind.size() > 0) {
            TrackDescriptionEvent event = data.events.get(eventIndex);
            eventIndex--;

            if (event.beat > beat) {
                // happens after our point, we don't care
                continue;
            }
            if (!toFind.contains(event.device)) {
                // already found an event for that device
                continue;
            }

            toFind.remove(event.device);
            list.add(event);
        }

        return list;
    }

    List<TrackDescriptionEvent> getEventsIntersectingBeats(double beatA, double beatB) {
        ArrayList<TrackDescriptionEvent> list = new ArrayList<>();
        if (data == null || data.events.size() == 0) {
            extension.println("No events");
            return list;
        }

        int intBeat = (int) beatA;
        int eventIndex = knownBeatIndexes.getOrDefault(intBeat, 0);

        while (eventIndex < data.events.size()) {
            TrackDescriptionEvent event = data.events.get(eventIndex);
            eventIndex++;

            if (event.beat < beatA) {
                // happens before the start, keeping going forward
                continue;
            }
            if (event.beat > beatB) {
                // we passed the last event that was within this region, stop!
                break;
            }

            list.add(event);
        }

        return list;
    }

    class TrackDescriptionData {
        List<TrackDescriptionEvent> events;
        TrackDescriptionData() {

        }
    }

    class TrackDescriptionEvent {
        String type = "arm";
        String trackName;
        String device;
        String id;
        double beat;

        TrackDescriptionEvent() {

        }
    }
}
